package controllers

import (
	"coding.com/personalBlog/models"
	"github.com/astaxie/beego"
	"path"
	"strings"
)

type TopicController struct {
	beego.Controller
}

func (this *TopicController) Get() {
	this.Data["IsLogin"] = checkAccount(this.Ctx)
	this.Data["IsTopic"] = true
	this.TplName = "topic.html"

	topics, err := models.GetAllTopics("", "", false)

	if err != nil {
		beego.Error(err.Error)
	} else {
		this.Data["Topics"] = topics
	}
}
func (this *TopicController) Post() {
	if !checkAccount(this.Ctx) {
		this.Redirect("/login", 302)
		return
	}

	// 解析表单
	title := this.Input().Get("title")
	content := this.Input().Get("content")
	tid := this.Input().Get("tid")
	category := this.Input().Get("category")
	label := this.Input().Get("label")

	// 获取附件名
	_, fh, err := this.GetFile("attachment")
	if err != nil {
		beego.Error(err)
	}
	// 保存附件
	var attachment string
	if fh != nil {
		attachment = fh.Filename
		beego.Info(attachment)
		err = this.SaveToFile("attachment", path.Join("attachment", attachment))
		if err != nil {
			beego.Error(err)
		}
	}

	if len(tid) == 0 {
		err = models.AddTopic(title, label, category, content, attachment)
	} else {
		err = models.ModifyTopic(tid, label, title, category, content, attachment)
	}

	if err != nil {
		beego.Error(err)
	}
	this.Redirect("/topic", 302)
}

func (this *TopicController) Add() { // 当路由是topic/add时会访问 topic_add.html 页面
	this.TplName = "topic_add.html"
	this.Data["IsLogin"] = checkAccount(this.Ctx)
}

func (this *TopicController) View() {
	this.TplName = "topic_view.html"

	tid := this.Ctx.Input.Param("0")
	// 也可以通过获取url来获取参数
	//reqUrl := this.Ctx.Request.RequestURI
	//i := strings.LastIndex(reqUrl, "/")
	//tid := reqUrl[i+1:]
	
	topic, err := models.GetTopic(tid) // this.Ctx.Input.Params beego将路由信息保存在了params中
	if err != nil {
		beego.Error(err)
		this.Redirect("/", 302)
		return
	}
	this.Data["Topic"] = topic
	this.Data["Labels"] = strings.Split(topic.Labels, " ")
	//this.Data["Tid"] = this.Ctx.Input.Param("0")

	// 显示评价列表
	replies, err := models.GetAllReplies(tid)
	if err != nil {
		beego.Error(err)
		return
	}
	this.Data["Replies"] = replies
	this.Data["IsLogin"] = checkAccount(this.Ctx)
}

func (this *TopicController) Modify() {
	this.TplName = "topic_modify.html"
	this.Data["IsLogin"] = checkAccount(this.Ctx)

	tid := this.Input().Get("tid")
	topic, err := models.GetTopic(tid)

	if err != err {
		beego.Error(err)
		this.Redirect("/", 302)
		return
	}

	this.Data["Topic"] = topic
	this.Data["Tid"] = tid
}

func (this *TopicController) Delete() {
	if !checkAccount(this.Ctx) {
		this.Redirect("/", 302)
		return
	}
	//this.Input().Get("key") // 对应url?之后的值
	//this.Ctx.Input.Param("0") //对应地址之后的值，比如topic/view/1 topic/view是地址，1是参数
	err := models.DeleteTopic(this.Input().Get("tid"))
	if err != nil {
		beego.Error(err)
	}

	this.Redirect("/", 302)
}
