package main

import (
	"coding.com/personalBlog/controllers"
	"coding.com/personalBlog/models"
	_ "coding.com/personalBlog/routers"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"os"
)

func init() {
	//注册数据库
	models.RegisterDB()
}
func main() {
	// 开启orm  调试模式
	orm.Debug = true
	// 自动建表
	orm.RunSyncdb("default", false, true)

	// 注册beego路由
	beego.Router("/", &controllers.MainController{})
	beego.Router("/login", &controllers.LoginController{})
	beego.Router("/category", &controllers.CopicController{})
	beego.Router("/topic", &controllers.TopicController{})
	beego.AutoRouter(&controllers.TopicController{}) //自动路由，当访问TopicController所对应的路由地址时，不默认访问get方法，而是使用对应的方法，比如本例用的Add，直接访问topic/add路由
	beego.Router("/reply", &controllers.ReplyController{})
	beego.Router("/reply/add", &controllers.ReplyController{}, "Post:Add")
	beego.Router("/reply/delete", &controllers.ReplyController{}, "Get:Delete")

	// 创建附件目录
	os.Mkdir("attachment", os.ModePerm)
	// 作为静态文件
	//beego.SetStaticPath("/attachment", "attachment")
	// 作为单独一个控制器来处理
	beego.Router("/attachment/:all", &controllers.AttachController{})
	
	// 启动beego
	beego.Run()
}
