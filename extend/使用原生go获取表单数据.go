package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func main() {
	http.HandleFunc("/", Hey)
	http.ListenAndServe(":8080", nil)
}

const tpl = `
<html>
	<head>
		<title>Key</title>
		<body>
			<form method="post" action="/">
				username: <input type="text" name="username" />
				password: <input type="password" name="password" />
				<button type="submit">提交</button>
			</form>
		</body>
	</head>
</html>
`

func Hey(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		t := template.New("key")
		t.Parse(tpl)
		t.Execute(w, nil) //数据流
	} else {
		fmt.Println(r.FormValue("username"))
	}
}
